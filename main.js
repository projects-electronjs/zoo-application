// ===== Variable et appelle framework electron =====
const {app, BrowserWindow} = require('electron');
const Menu = require('electron').Menu;
let win;

// ===== Fonction de création de fenetre =====
function createWindow () {

    win = new BrowserWindow({
        width: 1024, 
        height: 768,
        webPreferences: {
            nodeIntegration: true
        },
        resizable: false
    });

    win.loadFile(`app/index/index.html`);
    //win.webContents.openDevTools()

}

// ===== Methode d'ouverture d'application =====
app.whenReady().then(createWindow);


// ===== Menu de l'application =====
const template = [
    {
        label: 'Fichier',
        submenu: [
            {
                label: 'Quitter', 
                click: () => {app.quit()},
                accelerator: 'CmdOrCtrl+Q'
            }
        ]
    },{
        label: 'Édition',
        submenu: [
            {
                label: 'Copier',
                accelerator: 'CmdOrCtrl+C',
                selector: "copy:" 
            },{ 
                label: "Coller", 
                accelerator: "CmdOrCtrl+V", 
                selector: "paste:" 
            }
        ]
    }
];
const menu = Menu.buildFromTemplate(template);
Menu.setApplicationMenu(menu);