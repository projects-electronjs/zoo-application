const remote = require('electron').remote;

// ===== Evenement sur le lien closeWindow =====
document.querySelector(`#close-window`).addEventListener('click', () => window.close())

// ===== Fonctions de récuperation de donnée du formulaire =====
document.querySelector('#button-validation').addEventListener('click', () => {
    
    let newElements = {
        nom: document.querySelector(`input#nom`).value,
        taille: document.querySelector(`input#taille`).value,
        poids: document.querySelector(`input#poids`).value,
        description: document.querySelector(`input#description`).value,
        lienPhoto: document.querySelector(`input#lien-photo`).value
    }

    let arrayElements = [];

    if(localStorage.getItem('elements') !== null){
        arrayElements = JSON.parse(localStorage.getItem('elements'))
    }

    arrayElements.push(newElements);

    localStorage.setItem('elements', JSON.stringify(arrayElements));

    remote.getCurrentWindow().getParentWindow().send('element-added')
    
    alert(`L'animal ${newElements.nom} a bien été ajouté.`)
    window.close()
})
