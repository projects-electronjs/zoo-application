// ===== Variable et appelle framework electron =====
const {BrowserWindow, app} = require('electron').remote;
const remote = require('electron').remote;
const {ipcRenderer} = require('electron');
let winFormulaire;

// ===== Fonctions de création de la fenetre formulaire =====
document.querySelector('#open-formulaire').addEventListener('click', () => {

    winFormulaire = new BrowserWindow({
        width: 800, 
        height: 600,
        webPreferences: {
            nodeIntegration: true
        },
        parent: remote.getCurrentWindow(),
        backgroundColor: '#2f3237',
        resizable: false
    });

    winFormulaire.loadFile(`app/formulaire/formulaire.html`);
    //winAddElement.webContents.openDevTools()
})

// ===== Fonctions d'ajout d'élément dans index.html =====
ipcRenderer.on(`element-added`, elementAdded)
app.whenReady().then(elementAdded);

function elementAdded(){

    let recoverDatasParse = JSON.parse(localStorage.getItem('elements'));

    const elementContainer = document.querySelector('ul#liste-animaux');

    if(localStorage.getItem('elements') === null){

        let html = `<p>Il n'y a pas d'animal pour l'instant.</p>`
        elementContainer.innerHTML += html;

    }else{
       
        elementContainer.innerHTML = "";

        for(let recoverData of recoverDatasParse){
            let html = "";
            html = 
            `<li id="card" class="card">
                <img src="${recoverData.lienPhoto}" alt="${recoverData.nom}">
                <div>
                    <h5>${recoverData.nom}</h5>
                    <p>Taille : ${recoverData.taille} cm</p>
                    <p>Poids : ${recoverData.poids} kg</p>
                    <p>Description : ${recoverData.description}</p>
                </div>
            </li>`;
            elementContainer.innerHTML += html;
        }
    }
}